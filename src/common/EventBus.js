import Vue from 'vue';

export const EventBus = new Vue({
    methods:{
        sendNewData($payload){
            console.log('EventBus ==> sendNewData...', $payload)
            let newData;
            const jsonUrl = 'https://my-json-server.typicode.com/jongjin7/demo/system_control';
            fetch(jsonUrl, {
                method: 'POST',
                body: JSON.stringify({
                    plant: 'foo',
                    equipment: 'bar',
                    is_available: true
                }),
                headers: {
                    "Content-type": "application/json; charset=UTF-8"
                }
            })
            .then(response => response.json())
            .then(json => {
                //console.log(json)
                this.$emit('reloadData', json);
            });

            // fetch(jsonUrl)
            //     .then(response => response.json())
            //     .then(json => console.log(json))


        },
        toggleEditMode(){
            this.$emit('changeEditMode');
        },

        hexToRgba( hexCode, alpha){
            var hex = hexCode.replace( "#", "" );
            var value = hex.match( /[a-f\d]/gi );

            // 헥사값이 세자리일 경우, 여섯자리로.
            if ( value.length == 3 ) hex = value[0] + value[0] + value[1] + value[1] + value[2] + value[2];


            value = hex.match( /[a-f\d]{2}/gi );

            var r = parseInt( value[0], 16 );
            var g = parseInt( value[1], 16 );
            var b = parseInt( value[2], 16 );

            var rgbType = "rgb(" + r + ", " + g + ", " + b +','+ alpha + ")";

            return rgbType;
        } ,
    }
});

