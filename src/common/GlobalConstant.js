//chart모듈 Style정의
const CHART_STYLE = {
    fontEng:'Noto Sans',
    fontKor:'Noto Sans KR',
    colors:{
        basic:['69,72,76','#45484c'],
        white:['255,255,255', '#fff'],
        black:['0,0,0', '#000'],
        darkGray:['52,55,66', '#343742'], //도넛차트에서 없는 값 표현을 위해
        darkGray_01:['42,45,54', '#2a2d36'], //게이지차트
        darkGray_02:['86,92,102', '#565c66'], //게이지차트
        darkGray_03:['55','63','82', '#373f52'],
        gray_01:['138,148,161', '#8a94a1'], //게이지차트
        gray_02:['189,193,198', '#bdc1c6'],
        bgA:['30,193,201','#1ec1c9'],
        bgB:['66,193,84','#39b84a'],
        bgB_01:['56,162,67','#38a243'], //게이지
        bgC:['172,126,250','#ac7efa'],
        bgC_01:['142,83,190','#8e53be'],
        bgD:['114,119,221','#7277dd'],
        bgE:['255,0,222','#ff00de'], //overColor
        bgF:['250,88,126','#fa587e'], //overColor
        bgG:['255,139,38', '#ff8b26'],
        bgG_01:['217,145,56', '#d99138'],
        bgH:['224,78,79', '#e04e4f'],
    },
    tooltip:{
        fontSize:12,
    },
    chartJsModule:{
        axesLabelColor:'#8a94a1',
        axesThickColor:'#8a94a1',
        axesThickFontSize:11,
        axesThickPadding:10,

        xAxesThickMarkerSize:7,
        xAxesLineColor:'#8a94a1',

        yAxesLineColor:'#3e424d',
        yAxesZeroLineColor:'#8a94a1',
    }
};

const PAGE_COMPONENT_DATA = {
    main: {},
    // 시스템 현황
    system: {
        StatusEnergy: {
            pageCode: 'page-code-0101',
            mainComponentName: 'StatusEnergyFrameComponent',
        },
        StatusPcs: {
            pageCode: 'page-code-0103',
            mainComponentName: 'StatusPcsFrameComponent',
        }
    },
    //시스템 제어관리
    control:{
        ControlChange:{
            pageCode: 'page-code-0201',
            mainComponentName:'ControlChangeFrameComponent',
        },
        ControlChangeHistory:{
            pageCode: 'page-code-0202',
            mainComponentName:'ControlChangeHistoryFrameComponent',
        },
        ControlDevice:{
            pageCode: 'page-code-0204',
            mainComponentName:'ControlDeviceFrameComponent',
        }
    },
}

export { CHART_STYLE, PAGE_COMPONENT_DATA } ;
