const primaryDepthLinks = [
    {
        title:'시스템현황',
        link:'/system',
        iconName:'shuffle',
    },
    {
        title:'시스템제어관리',
        link:'/control',
        iconName:'gear',
    },
    // {
    //     title:'통계분석',
    //     link:'/analysis',
    //     iconName:'cross-square',
    // },
    // {
    //     title:'운영',
    //     link:'/operation',
    //     iconName:'line-dot',
    // },
    //
    // {
    //     title:'관리',
    //     link:'/management',
    //     iconName:'monitor',
    // }
];


const secondaryDepthLinks = {
    system:{
        primaryTitle:primaryDepthLinks[0].title,
        tabMenuLinks:[
            {
                title:'에너지현항',
                link:'StatusEnergy'
            },
            {
                title:'PCS현황',
                link:'StatusPcs'
            },
            // {
            //     title:'PCS현항',
            //     link:'/system/chart'
            // },
            // {
            //     title:'BMS현항',
            //     link:'/system/etc'
            // },
            // {
            //     title:'EMS현항',
            //     link:'/system/etc'
            // }
        ]
    },
    control:{
        primaryTitle:primaryDepthLinks[1].title,
        tabMenuLinks:[
            {
                title:'충·방전 제어',
                link:'ControlChange'
            },
            {
                title:'충·방전 제어 이력',
                link:'ControlChangeHistory'
            },
            // {
            //     title:'환경설정',
            //     link:'/system/chart'
            // },
            {
                title:'장비등록관리',
                link:'ControlDevice'
            }
        ]
    }

};



export { primaryDepthLinks, secondaryDepthLinks };

