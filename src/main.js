import Vue from 'vue'
import App from './App.vue'
import router from './router/router'
import store from './store'
import './registerServiceWorker'


import './assets/scss/style.scss';

import BootstrapVue from 'bootstrap-vue';
Vue.use(BootstrapVue);

// icons

import { library } from '@fortawesome/fontawesome-svg-core'
import { faUserSecret, faSignOutAlt } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
library.add( faUserSecret, faSignOutAlt )
Vue.component('font-awesome-icon', FontAwesomeIcon)

// custom scrollbar
import Vuebar from 'vuebar';
Vue.use(Vuebar);

//window.$ = window.jQuery = require("jquery");
import 'expose-loader?$!expose-loader?jQuery!jquery'

import 'bootstrap-datepicker/dist/js/bootstrap-datepicker.min'
import 'bootstrap-datepicker/dist/css/bootstrap-datepicker.standalone.min.css'

// 글로벌 상수
import { EventBus } from './common/EventBus';

import GlobalComponent from './views/components/globalComponent'
Vue.component(GlobalComponent.name, GlobalComponent)


const moment = require('moment');
Vue.prototype.moment = moment();

Vue.prototype.$EventBus = EventBus;

Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
