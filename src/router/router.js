import Vue from 'vue'
import Router from 'vue-router'

import MainComponent from '@/views/layout/page-frame/MainPageComponent'
import SubPageComponent from "@/views/layout/page-frame/SubPageComponent"
import AccountPageComponent from '@/views/layout/page-frame/AccountPageComponent'

import AccountLogin from '@/views/pages/accounts/AccountLogin'
import AccountFindId from '@/views/pages/accounts/AccountFindId'
import AccountLoginAfterFindId from '@/views/pages/accounts/AccountLoginAfterFindId'
import AccountFindPasswordSendMail from '@/views/pages/accounts/AccountFindPasswordSendMail'
import AccountSignUp from '@/views/pages/accounts/AccountSignUp.vue'
import AccountCompleteRegisterUser from '@/views/pages/accounts/AccountCompleteRegisterUser'
import AccountUpdatePassword from '@/views/pages/accounts/AccountUpdatePassword'





Vue.use(Router)

const UserHome = { template: '<div>Home</div>' }

export default new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/',
            name: 'home',
            component: MainComponent
        },
        {
            path: '/sub',
            name: 'sub',
            component: UserHome,
            //redirect: '/sub/sub1',
            //props: (route) => ({ query: route.query.q }),
            // children: [
            //     {
            //         name:'subPg1',
            //         path: 'sub1',
            //         //component: SubPageComponent
            //     },
            //     {
            //         name:'subPg2',
            //         path: 'sub2',
            //         //component: SubPageComponent
            //     }
            // ],
        },
        {
            path: '/sub/:page',
            name: 'sub-child',
            component: SubPageComponent
        },
        {
            path: '/system/',
            name: 'system page',
            redirect: '/system/StatusEnergy',
            component: SubPageComponent,
            children:[
                {
                    path: 'StatusEnergy',
                    name: 'system-page1',
                    //component: RouterTest,
                },
                {
                    path: 'StatusPcs',
                    name: 'pcs-page',
                    //component: ohterRouter,
                }
            ]
        },
        {
            path: '/control/',
            name: 'control-page',
            redirect: '/control/ControlChange',
            component: SubPageComponent,
            children:[
                {
                    path: 'ControlChange',
                    name: 'control-page-1',
                },
                {
                    path: 'ControlChangeHistory',
                    name: 'control-page-2',
                },
                {
                    path: 'ControlDevice',
                    name: 'control-page-3',
                }
            ]
        },
        {
            path: '/account',
            name: 'Account',
            component: AccountPageComponent,
            redirect: '/',
            children: [
                {
                    // /Accounts/login 과 일치 할 때
                    // UserProfile은 User의 <router-view> 내에 렌더링 됩니다.
                    path: 'login',
                    component: AccountLogin,
                    props: {page: 'login'}
                },
                {
                    path: 'find_id',
                    component: AccountFindId,
                    props: {page: 'find_id'}
                },
                {
                    path: 'find_id_login',
                    component: AccountLoginAfterFindId,
                    props: {page: 'find_id_login'}
                },
                {
                    path: 'find_password_send_mail',
                    component: AccountFindPasswordSendMail,
                    props: {page: 'find_password_send_mail'}
                },
                {
                    path: 'update_password',
                    component: AccountUpdatePassword,
                    props: {page: 'update_password'}
                },
                {
                    path: 'sign_up',
                    component: AccountSignUp,
                    props: {page: 'sign_up'}
                },
                {
                    path: 'complete_register_user',
                    component: AccountCompleteRegisterUser,
                    props: {page: 'complete_register_user'}
                },

            ]
        },

    ]
})
