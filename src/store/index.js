import Vue from 'vue'
import Vuex from 'vuex'

import mutations from './mutations'
import actions from './actions'

import datatable from './modules/datatable'

Vue.use(Vuex);

const state = {
    gap: 'my jongin'
};

const getters = {
    rootGetterGap(){
        return state.gap
    }
};



export default new Vuex.Store({
    state,
    getters,
    mutations,
    actions,

    modules: {
        datatable,
    },
    strict: process.env.NODE_ENV !== 'production'
})