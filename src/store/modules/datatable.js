import Constant from '@/common/VuexConstant'

const state = {
    datatableGap:'my_data',
    price:1000,
};

const getters = {
    originalPrice(state) {
        return state.price;
    },
    doublePrice(state) {
        return state.price * 2;
    },
    triplePrice(state) {
        return state.price * 3;
    }
};

const mutations = {
    changeValue(){
        state.datatableGap = '변경된 값'
    }
};

const actions = {
    testStoreAction(){
        console.log('datasotre default:::', state.datatableGap)
    },

    storeChangeValue({commit}){
        commit('changeValue');
    }



};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
