import { Doughnut } from 'vue-chartjs'

export default {
    extends: Doughnut,
    props:{
        chartdata: {
            type: Object,
            default: null
        },
        chartoptions: {
            type: Object,
            default: null
        }
    },
    data () {
        return {
            defaultDatas: {},
            defaultOptions: {
                responsive: true,
                legend:{
                    display:false,
                },
                cutoutPercentage: 70, //도넛 구멍 크기
                tooltips: {
                    enabled:false,
                }
            }
        }
    },
    created(){

    },
    mounted () {
        let newOptions = this.chartoptions !== null? {...this.defaultOptions, ...this.chartoptions} : this.defaultOptions;

        //console.log('doughnut-chart:::', this.chartdata, newOptions)
        this.renderChart(this.chartdata, newOptions )
    }
}
