
//Importing Line class from the vue-chartjs wrapper
import {Line} from 'vue-chartjs'
import { CHART_STYLE } from '@/common/GlobalConstant'
import { EventBus } from "@/common/EventBus"
//Exporting this so it can be used in other components


export default {
    extends: Line,
    props:{
        chartdata: {
            type: Object,
            default: null
        },
        chartoptions: {
            type: Object,
            default: null
        },
        //htmls: String,

    },
    data () {
        return {
            //htmlLegend:null,

            defaultDatas: {
                //Data to be represented on x-axis
                /*labels: ['00','01','02','03','04','05','06','07','08','09','10','11','12','13','14','15','16','17','18','19','20','21','22','23', '24'],
                datasets: [
                    {
                        label: "태양광발전량",
                        backgroundColor: '#f87979',
                        pointBackgroundColor: 'white',
                        borderWidth: 1,
                        pointBorderColor: '#249EBF',
                        pointRadius: 0,
                        //Data to be represented on y-axis
                        data: [40, 20, 30, 50, 90, 10, 20, 40, 50, 70, 90, 100]
                    },
                    {
                        label: "충전량",
                        backgroundColor: '#090988',
                        pointBackgroundColor: 'white',
                        pointRadius: 0,
                        borderWidth: 1,
                        pointBorderColor: '#e0e111',
                        //Data to be represented on y-axis
                        data: [70, 80, 10, 50, 90, 10, 20, 50, 50, 70, 10, 100]
                    }
                ]*/
            },
            //Chart.js options that controls the appearance of the chart
            defaultOptions: {
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            fontColor:'#8a94a1',
                            fontFamily:'Noto Sans KR',
                            //labelString: 'Month',
                        },
                        gridLines: {
                            //display: false,
                            drawBorder:true,
                            drawTicks:true, //Y축 첫번째 라인
                            drawOnChartArea:false, //X축 생성
                            tickMarkLength:7,
                            color:'#8a94a1',
                            lineWidth:1,
                        },
                        ticks:{
                            //display:false,
                            padding:10,
                            fontColor:'#8a94a1',
                            fontSize: 11,
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            fontColor:'#8a94a1',
                            fontFamily:'Noto Sans KR',
                            //labelString: 'Value'
                        },
                        gridLines: {
                            display: true,
                            drawTicks:false,
                            drawBorder:false,
                            color:'#3e424d',
                            zeroLineWidth:1, //Y축 절대 중앙점
                            zeroLineColor:'gray'
                        },
                        ticks: {
                            //min:1,
                            //max: 30,//min: 0
                            beginAtZero: true,
                            stepSize:5,
                            fontColor:'#8a94a1',
                            fontSize: 11,
                            padding:10,
                        }
                    }]
                },
                tooltips:{
                    //display: false,
                    backgroundColor:EventBus.hexToRgba(CHART_STYLE.colors.white[1], 0.85),
                    borderColor:EventBus.hexToRgba(CHART_STYLE.colors.darkGray_02[1], 0.85),
                    borderWidth:2,
                    cornerRadius:0,
                    displayColors:false,
                    titleFontFamily:CHART_STYLE.fontKor,
                    titleFontColor:CHART_STYLE.colors.basic[1],
                    titleFontStyle:'normal',
                    bodyFontColor:CHART_STYLE.colors.bgD[1],
                    bodyFontFamily:CHART_STYLE.fontKor,
                },
                legend: {
                    display: false,
                    align:'end',
                    labels:{
                        fontColor:'#a4b0bf',
                        fontFamily:CHART_STYLE.fontKor,

                    },
                },
                // legendCallback: function(chart) {
                //     // Return the HTML string here.
                //     console.log('lengendCallback', chart)
                // },
                responsive: true,
                maintainAspectRatio: false
            }
        }
    },
    created() {

    },
    mounted () {
        //renderChart function renders the chart with the datacollection and options object.

        let newOptions = this.chartoptions !== null? {...this.defaultOptions, ...this.chartoptions} : this.defaultOptions;



        newOptions.scales.xAxes[0].scaleLabel.labelString = newOptions.customOption.xAxislabelString;
        newOptions.scales.yAxes[0].scaleLabel.labelString = newOptions.customOption.yAxislabelString;
        newOptions.legend.display = newOptions.customOption.legend.display ? true : false;
        //console.log('lineChart',this.defaultOptions,newOptions)
        this.renderChart(this.chartdata, newOptions )

        //this.htmlLegend = this.generateLegend()
        //this.$emit('update:htmls', this.htmlLegend);
    },
    methods:{

    }
}