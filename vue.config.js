const path = require('path')

module.exports = {
    configureWebpack: {
        devtool: 'source-map',
        resolve: {
            alias: {
                'bootstrap-components': path.resolve(__dirname, 'node_modules/bootstrap-vue/es/components'),
             }
        },

    },
    // publicPath:'./',
    // assetsDir:'./',
    css: {
        sourceMap: true,
        extract: false,
    },
    productionSourceMap: true
}
